﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class SenderEventTrigger : MonoBehaviour 
{
	[SerializeField]
	public UnityEvent onStartEvent = new UnityEvent ();

	// Use this for initialization
	void Start () 
	{
		onStartEvent.Invoke();
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
