﻿using UnityEngine;
using System.Collections;

public class AnimationTutorial : MonoBehaviour {

	Animator anim;
	void Start(){
		anim = transform.GetComponent<Animator> ();

	}

	void Update(){

		if (Input.GetKey (KeyCode.W)) {

			anim.SetInteger ("KeyMove", 1);

		}
		if (Input.GetKey (KeyCode.S)) {

			anim.SetInteger ("KeyMove", -1);

		}

		if (Input.GetKey (KeyCode.Space)) {

			anim.SetInteger ("KeyMove", 0);

		}

	}

}